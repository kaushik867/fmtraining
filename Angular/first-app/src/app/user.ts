export class User {
    constructor(
        public Name: string,
        public Dob: string,
        public Email: string,
        public Phone: number,
        public Password: string,
        public ConPass: string
    ){}
}
