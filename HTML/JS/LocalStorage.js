
var Arr = new Array();
function AddData () {
    GetData();
    CheckName = ValidateName(document.getElementById("Name").value);
    CheckDob = ValidateDob(document.getElementById("Dob").value);
    CheckEmail = ValidateEmail(document.getElementById("Email").value);
    CheckUserId = ValidateUserId(document.getElementById("UserId").value);
    CheckPass = ValidatePass(document.getElementById("Password").value)
    CheckConPass = ValidateConPass(document.getElementById("ConPass").value , document.getElementById("Password").value)
    CheckPhoneNo = ValidateNumber(document.getElementById("Phone").value)
    if( CheckName && CheckEmail && CheckDob && CheckUserId && CheckPass && CheckConPass && CheckPhoneNo )
    { 
        Arr.push({
        Name: document.getElementById("Name").value,
        Dob: document.getElementById("Dob").value,
        Email: document.getElementById("Email").value,
        UserId: document.getElementById("UserId").value,
        Phone: document.getElementById("Phone").value
        });
    localStorage.setItem("LocalData",JSON.stringify(Arr));
    ShowData();
    }
}

function GetData(){
    var Str = localStorage.getItem("LocalData");
    if(Str != null)
        Arr = JSON.parse(Str);
}


function ShowData(){
    GetData();
    var Table = document.getElementById("LocalDataTbl");
    var x = Table.rows.length;
    while(--x){
        Table.deleteRow(x);
    }
    for( var Index = 0 ; Index < Arr.length ; Index++)
    {
        var Row = Table.insertRow();
        var col1 = Row.insertCell();
        var col2 = Row.insertCell();
        var col3 = Row.insertCell();
        var col4 = Row.insertCell();
        var col5 = Row.insertCell();
        col1.textContent = Arr[Index].Name;
        col2.textContent = Arr[Index].Dob;
        col3.textContent = Arr[Index].Email;
        col4.textContent = Arr[Index].UserId;
        col5.textContent = Arr[Index].Phone;
    }
    document.getElementById("RegistrationForm").reset();
}
function DeleteData(){
    localStorage.clear();
    location.reload();
}

function ValidateName(value){
    var NameRegEx = /^[a-zA-Z ]+[,\.'\-]?[a-zA-Z]+$/;
    if(value == "")
    {
       return SetError("NameError", "Name" ,"Field can't be Empty")
    }
    else if(!NameRegEx.test(value))
    {
        return SetError("NameError", "Name" ,"Enter a Valid Name")
    }
    else{
        return RemoveError("NameError", "Name");
    }
}
function ValidateDob(value){
    if(value == "")
    {
        return SetError("DobError" , "Dob" ,"Field can't be Empty");
    }
    else
    {
        var today = new Date();
        var Data = value.split("-");
        var FormDate = new Date(Data[0],Data[1]-1,Data[2])
        if(today > FormDate) 
            return RemoveError("DobError" , "Dob");
        else
            return SetError("DobError" , "Dob" ,"Enter a valid Date");
    }
}
function ValidateEmail(value){
    var EmailRegEx = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if(value == "")
    {
        return SetError("EmailError" , "Email" ,"Field can't be Empty");
    }
    else if(!EmailRegEx.test(value))
    {
        return SetError("EmailError" , "Email" ,"Enter a valid Email");
    }
    else
    {
        GetData();
        for(var Index = 0 ; Index < Arr.length ; Index++)
        {
            if(Arr[Index].Email == value)
            {
                return SetError("EmailError" , "Email" ,"Email Already Exist");
            }
        }
        return RemoveError("EmailError" , "Email");
    }
}
function ValidateUserId(value){
    var userIdRegEx = /^[a-zA-Z]+[0-9]*$/;
    if(value == "")
    {
        return SetError("UserIdError" , "UserId" ,"Field can't be Empty");
    }
    else if(!userIdRegEx.test(value))
    {
        return SetError("UserIdError" , "UserId" ,"Enter a valid Email Id");
    }
    else
    {
        GetData();
        for(var Index = 0 ; Index < Arr.length ; Index++)
        {
            if(Arr[Index].UserId == value)
            {
                return SetError("UserIdError" , "UserId" ,"User Id already exit");
            }
        }
        return RemoveError("UserIdError" , "UserId");
    }
}
function ValidateNumber(value){
    var PhoneRegEx = /^[0-9]{10}$/;
    if(value == "")
    {
        return SetError("PhoneError" , "Phone" ,"Field can't be Empty");
    }
    else if(!PhoneRegEx.test(value))
    {
        return SetError("PhoneError" , "Phone" ,"Enter a valid phone number");
    }
    else
    {
        GetData();
        for(var Index = 0 ; Index < Arr.length ; Index++)
        {
            if(Arr[Index].Phone == value)
            {
                return SetError("PhoneError" , "Phone" ,"Phone Number already exit");
            }
        }
        return RemoveError("PhoneError" , "Phone");
    }
}
function ValidatePass(value){
    var PassRegEx = /^(?=.*[0-9])(?=.*[&*$#%@!^~])[a-zA-Z0-9&*$#%@!^~]{8,}$/;
    if(value == "")
    {
        return SetError("PassError" , "Password" ,"Field can't be Empty")
    }
    else if(!PassRegEx.test(value))
    {
        return SetError("PassError" , "Password" ,"Enter a valid password")
    }
    else
    {
        return RemoveError("PassError" , "Password");
    }
}
function ValidateConPass(value , password)
{
    if(value == "")
    {
        return SetError("ConPassError" , "ConPass" ,"Field can't be Empty");
    }
    else if( value != password)
    {
        return SetError("ConPassError" , "ConPass" ,"Password can not matched");
    }
    else
    {
        return RemoveError("ConPassError" , "ConPass");
    }
}

function SetError(Id , InputId , ErrorMsg)
{
    document.getElementById(Id).innerHTML = ErrorMsg;
    document.getElementById(InputId).style.borderBottom = "1px solid #FF0000";
    document.getElementById(Id).style.visibility = "visible";
    return false;
}

function RemoveError(Id , InputId)
{
    document.getElementById(Id).style.visibility = "hidden";
    document.getElementById(InputId).style.borderBottom = "1px solid #C0C0C0";   
    return true;
}

function CheckError(Element){

    var Id =Element.id;
    if(Id == "Name")
        CheckName = ValidateName(document.getElementById("Name").value);
    else if(Id == "Dob")
        CheckDob = ValidateDob(document.getElementById("Dob").value);
    else if( Id == "Email")
        CheckEmail = ValidateEmail(document.getElementById("Email").value);
    else if( Id == "UserId")
        CheckUserId = ValidateUserId(document.getElementById("UserId").value);
    else if( Id == "Password")
        CheckPass = ValidatePass(document.getElementById("Password").value)
    else if( Id == "ConPass")
        CheckConPass = ValidateConPass(document.getElementById("ConPass").value , document.getElementById("Password").value)
    else if(Id == "Phone")
        CheckPhoneNo = ValidateNumber(document.getElementById("Phone").value)
}