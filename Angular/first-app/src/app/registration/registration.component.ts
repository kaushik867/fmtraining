import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import * as CryptoJS from 'crypto-js';
import Swal from 'sweetalert';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {

  userModel = new User("","","",undefined,"","");
  constructor(private router: Router){}
  onSubmit(){
    var obj = this.userModel;
    var ErrorArray = new Array();
    for(var Keys in obj ){
      ErrorArray.push(ValiadateFields(Keys, obj[Keys] , this.userModel.Password));;
    }
   
    for( var Index in ErrorArray)
    {
      if( !ErrorArray[Index]){
        return false;
      }
    }
    var Arr = GetData();
    delete this.userModel.ConPass;
    this.userModel.Password = CryptoJS.SHA256(this.userModel.Password).toString();
    Arr.push(this.userModel);
    sessionStorage.setItem("Data",JSON.stringify(Arr));
    localStorage.setItem("userData",JSON.stringify(this.userModel));
    this.router.navigateByUrl('Welcome');
  }

  back(){
    this.router.navigate(['']);
  }

  onBlurEvent(event: any){
    ValiadateFields(event.target.id, event.target.value, this.userModel.Password);
 }

}
function ValiadateFields(id:string, Value:string, pass?:string) {
  var Arr=GetData();
   var NameRegEx = /^[a-zA-Z ]+[,\.'\-]?[a-zA-Z ]+$/;
   var EmailRegEx = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
   var PhoneRegEx = /^[0-9]{10}$/;
   var PassRegEx = /^(?=.*[0-9])(?=.*[&*$#%@!^~\s])[a-zA-Z0-9&*$#%@!^~ ]{8,}$/;
  if(!Value){
    document.getElementById(id).classList.add("is-invalid");
    return false;
  }
  else if(id == "Name" && !NameRegEx.test(Value.trim()) ){
    document.getElementById(id).classList.add("is-invalid");
    return false;
  }
  else if(id == "Email" && !EmailRegEx.test(Value.trim()) ){
    document.getElementById(id).classList.add("is-invalid");
    return false;
  }
  else if(id == "Phone" && !PhoneRegEx.test(Value) ){
    document.getElementById(id).classList.add("is-invalid");
    return false;
  }
  else if(id == "Password" && !PassRegEx.test(Value) ){
    document.getElementById(id).classList.add("is-invalid");
    return false;
  }
  else if(id == "ConPass")
  {
    if( Value != pass){
      document.getElementById(id).classList.add("is-invalid");
      return false;
    }
    else{
      document.getElementById(id).classList.remove("is-invalid");
      document.getElementById(id).classList.add("is-valid");
      return true;
    }
  }
  else{
    if(id=="Email" || id == "Phone")
    {
      for(var i=0; i<Arr.length;i++){
        if(Arr[i].Email == Value){
          document.getElementById(id).classList.add("is-invalid");
          document.getElementById("EmailErr").innerHTML="Email exist";
          return false;
        }
        else if(Arr[i].Phone == Value){
          document.getElementById(id).classList.add("is-invalid");
          document.getElementById("PhoneErr").innerHTML="Phone nuber exist";
          return false;
        }
      }
    }
    if(id == "Email")
      document.getElementById("EmailErr").innerHTML="";
   if(id == "Phone")
      document.getElementById("PhoneErr").innerHTML="";
  document.getElementById(id).classList.remove("is-invalid");
  document.getElementById(id).classList.add("is-valid");
  return true;
  }
  
}


  function GetData(){
    var Arr = [];
    var Str = sessionStorage.getItem("Data");
    if(Str != null){
      Arr = JSON.parse(Str);
      return Arr;
    }
    else
      return Arr;
}
