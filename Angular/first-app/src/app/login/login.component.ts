import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {
hasEmailError = false;
hasPassError = false;
constructor(private router: Router){}
onSubmit( data){
  var arr = GetData();
  if(data.Email == "")
    this.hasEmailError=true;
  if(data.Password == "")
    this.hasPassError=true;
  for(var Index = 0 ; Index < arr.length; Index++)
  {
    if(arr[Index].Email == data.Email && arr[Index].Password == CryptoJS.SHA256(data.Password).toString() )
    {
      this.router.navigateByUrl('Welcome');
      localStorage.setItem("userData", JSON.stringify(arr[Index]));
      return true;
    }
  }
  document.getElementById("LoginErr").innerHTML="Invalid Email or Password";
  this.hasEmailError = true;
  this.hasPassError = true;
  
}
checkEmail(data){
  if(data == "" )
    this.hasEmailError = true;
  else
    this.hasEmailError =false;
}
checkPass(data){
  if(data == "" )
    this.hasPassError = true;
  else
    this.hasPassError =false;
}
back(){
  this.router.navigate(['']);
}

}

function GetData(){
  var Arr = [];
    var Str = sessionStorage.getItem("Data");
    if(Str != null){
      Arr = JSON.parse(Str);
      return Arr;
    }
    else
      return Arr;
}