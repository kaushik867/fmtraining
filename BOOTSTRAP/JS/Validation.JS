var Arr = new Array();
var SelecetedElement;
var RowNumber;
$(document).ready(function(){
    
    $('[data-toggle="tooltip"]').tooltip();
    var Clear = document.getElementById("Clear");
    Clear.addEventListener("click" , function (){
    document.forms[0].reset();
    });
    var InputFields = document.getElementsByClassName("form-control");
    var Submit = document.getElementById("Submit");
    Submit.addEventListener("click", function(){
        RowNumber = null;
        var FormArray = new Array();
        for(var i=0; i<InputFields.length;i++){
            FormArray.push(ValidateData(InputFields[i]));
        }
       for(var i=0; i<FormArray.length;i++){
           if(!FormArray[i])
                return false;
       }
       swal({
        title: "Thank you!",
        text: "Registration success",
        icon: "success",
        button: "Ok",
      });
      SetData();
      for( var i=0 ; i<InputFields.length;i++)
        {
            InputFields[i].classList.remove("is-valid");
        }
    },false);

    
    for( var i=0 ; i<InputFields.length;i++)
    {
        InputFields[i].addEventListener('blur', function(){
           ValidateData(this);
        }, false);
    }

    var Table = document.getElementById("LocalData");
    Table.addEventListener("click", function(e){
        if(SelecetedElement != null)
            SelecetedElement.classList.remove("Clicked");
        SelecetedElement = e.target.parentElement;
        RowNumber = e.target.parentElement.rowIndex;
        if(RowNumber != 0){
            SelecetedElement.classList.add("Clicked");
        }
    });

    document.getElementById("ClearLocalData").addEventListener("click",ClearRecords);
    document.getElementById("Edit").addEventListener("click",EditData,false);
    document.getElementById("Delete").addEventListener("click", DeleteRow);
    document.getElementById("Discard").addEventListener("click",function(){
        document.getElementById("Submit").style.display = "inline-block";
        document.getElementById("Clear").style.display = "inline-block";
        document.getElementById("Save").style.display = "none";
        document.getElementById("Discard").style.display  = "none";
        document.forms[0].reset();
        var InputFields = document.getElementsByClassName("form-control");
        for( var i=0 ; i<InputFields.length;i++)
        {
            InputFields[i].classList.remove("is-invalid");
            InputFields[i].classList.remove("is-valid");
            RowNumber =null;
        }
    });
    document.getElementById("Save").addEventListener("click", function(){
        
        var FormArray = new Array();
        var row = RowNumber;
        for(var i=0; i<InputFields.length;i++){
            FormArray.push(ValidateData(InputFields[i]));
        }
        for(var i=0; i<FormArray.length;i++){
            if(!FormArray[i])
                    return false;
        }
        GetData();
       
        Arr.splice(row-1, 1 ,{
            Name: document.getElementById("Name").value,
            Dob: document.getElementById("Dob").value,
            Email: document.getElementById("Email").value,
            UserId: document.getElementById("UserId").value,
            Phone: document.getElementById("Phone").value,
            Password: document.getElementById("Password").value
            });
        localStorage.setItem("LocalData",JSON.stringify(Arr));
        ShowData();
        row="";
        document.getElementById("Save").style.display = "none";
        document.getElementById("Discard").style.display  = "none";
        setTimeout(function(){
            document.getElementById("Submit").style.display = "inline-block";
            document.getElementById("Clear").style.display = "inline-block";
        },500);
        document.forms[0].reset();
        RowNumber = null;
        for( var i=0 ; i<InputFields.length;i++)
        {
            InputFields[i].classList.remove("is-valid");
            InputFields[i].classList.remove("is-invalid");
        }
    });
})

function ValidateData(FormElement){
        
            if(FormElement.id == "Name")
            {
               var NameRegEx = /^[a-zA-Z ]+[,\.'\-]?[a-zA-Z ]+$/;
               if(FormElement.value == ""){
                    return SetError(FormElement);
               }
               else if(!NameRegEx.test(FormElement.value)) {
                    return SetError(FormElement);
               }
               else{
                    return RemoveError(FormElement);
               }
            }
            else if(FormElement.id == "Dob")
            {
                if(FormElement.value == ""){
                    return SetError(FormElement);
                }
                else 
                {
                    var today = new Date();
                    var Data = FormElement.value.split("-");
                    var FormDate = new Date(Data[0],Data[1]-1,Data[2]);
                    if(today > FormDate){ 
                        return RemoveError(FormElement);
                    }
                    else{
                        return SetError(FormElement);
                    }
                }
            }
            else if(FormElement.id == "Email")
            {
                var EmailRegEx = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
                if(FormElement.value == ""){
                    return SetError(FormElement);
                }
                else if(!EmailRegEx.test(FormElement.value)){
                    return SetError(FormElement);
                }
                else{
                    GetData();
                    for( var index = 0; index < Arr.length ;index++)
                    {
                        if(Arr[index].Email == FormElement.value && (RowNumber-1) != index){
                            return SetError(FormElement);
                        }
                    }
                    return RemoveError(FormElement);
                }
            }
            else if(FormElement.id == "Phone")
            {
                var PhoneRegEx = /^[0-9]{10}$/;
                if(FormElement.value == ""){
                    return SetError(FormElement);
                }
                else if(!PhoneRegEx.test(FormElement.value)){
                    return SetError(FormElement);
                }
                else{
                    GetData();
                    for( var index = 0; index < Arr.length ; index++){
                        if(Arr[index].Phone == FormElement.value && (RowNumber-1) != index)
                        {
                            return SetError(FormElement);
                        }
                    }
                    return RemoveError(FormElement);
                }
            }
            else if(FormElement.id == "UserId")
            {
                var userIdRegEx = /^[a-zA-Z]+[0-9]*$/;
                if(FormElement.value == ""){
                    return SetError(FormElement);
                }
                else if(!userIdRegEx.test(FormElement.value)){
                    return SetError(FormElement);
                }
                else{
                    GetData();
                    for( var index = 0; index < Arr.length ; index++){
                        if(Arr[index].UserId == FormElement.value && (RowNumber-1) != index)
                        {
                            return SetError(FormElement);
                        }
                    }
                    return RemoveError(FormElement);
                }
            }
            else if(FormElement.id == "Password")
            {
                var PassRegEx = /^(?=.*[0-9])(?=.*[&*$#%@!^~\s])[a-zA-Z0-9&*$#%@!^~ ]{8,}$/;
                if(FormElement.value == ""){
                    return SetError(FormElement);
                }
                else if(!PassRegEx.test(FormElement.value)){
                    return SetError(FormElement);
                }
                else{
                    return RemoveError(FormElement);
                }
            }
            else if(FormElement.id == "ConPass")
            {
                var Password = document.getElementById("Password").value;
                if(FormElement.value == ""){
                    return SetError(FormElement);
                }
                else if(FormElement.value != Password){
                    return SetError(FormElement);
                }
                else{
                    return RemoveError(FormElement);
                }
            }
}
function SetError(FormElement){
    FormElement.classList.add("is-invalid");
    FormElement.classList.remove("is-valid");
    return false;
}
function RemoveError(FormElement){
    FormElement.classList.remove("is-invalid");
    FormElement.classList.add("is-valid");
    return true;
}
function GetData(){
    var Str = localStorage.getItem("LocalData");
    if(Str != null)
        Arr = JSON.parse(Str);
    else
        Arr.splice(0, Arr.length);
}
function SetData(){
    GetData();
    Arr.push({
        Name: document.getElementById("Name").value,
        Dob: document.getElementById("Dob").value,
        Email: document.getElementById("Email").value,
        UserId: document.getElementById("UserId").value,
        Phone: document.getElementById("Phone").value,
        Password: document.getElementById("Password").value
        });
    localStorage.setItem("LocalData",JSON.stringify(Arr));
    ShowData();
    document.forms[0].reset();
}

function ShowData(){
    GetData();
    var Table = document.getElementById("LocalData");
    var x = Table.rows.length;
    while(--x){
        Table.deleteRow(x);
    }
    for( var Index = 0 ; Index < Arr.length ; Index++)
    {
        var Row = Table.insertRow();
        var col1 = Row.insertCell();
        var col2 = Row.insertCell();
        var col3 = Row.insertCell();
        var col4 = Row.insertCell();
        var col5 = Row.insertCell();
        col1.textContent = Arr[Index].Name;
        col2.textContent = Arr[Index].Dob;
        col3.textContent = Arr[Index].Email;
        col5.textContent = Arr[Index].Phone;
        col4.textContent = Arr[Index].UserId;
    }
}
setTimeout(function(){
    ShowData();
   }, 300);

function ClearRecords(){
    localStorage.clear();
    ShowData();
}

function EditData(){
    var InputFields = document.getElementsByClassName("form-control");
    for( var i=0 ; i<InputFields.length;i++)
    {
        InputFields[i].classList.remove("is-invalid");
    }
    ShowData();
    if(RowNumber)
    {
        
        document.getElementById("Name").value = Arr[RowNumber-1].Name;
        document.getElementById("Dob").value = Arr[RowNumber-1].Dob;
        document.getElementById("Email").value = Arr[RowNumber-1].Email;
        document.getElementById("Phone").value = Arr[RowNumber-1].Phone;
        document.getElementById("UserId").value = Arr[RowNumber-1].UserId;
        document.getElementById("Password").value = Arr[RowNumber-1].Password;
        document.getElementById("ConPass").value = Arr[RowNumber-1].Password;
        document.getElementById("Submit").style.display = "none";
        document.getElementById("Clear").style.display = "none";
        document.getElementById("Save").style.display = "inline-block";
        document.getElementById("Discard").style.display = "inline-block";

    }
}

function DeleteRow(){
    if(RowNumber)
    {
        GetData();
        Arr.splice(RowNumber-1,1);
        localStorage.setItem("LocalData",JSON.stringify(Arr));
        ShowData();
        RowNumber = null;
    }
}

