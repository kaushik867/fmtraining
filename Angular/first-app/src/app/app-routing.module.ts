import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegistrationComponent } from './registration/registration.component';
import { UserauthGuard } from './userauth.guard';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: '' , component: HomeComponent },
  { path: 'Login', component: LoginComponent , canActivate: [UserauthGuard]},
  { path: 'Register' , component: RegistrationComponent , canActivate: [UserauthGuard]},
  { path: 'Welcome' , component: WelcomeComponent , canActivate: [AuthGuard]},
  { path: '**' , component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }
